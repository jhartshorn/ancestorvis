// Initialize the map
const map = L.map('map').setView([54.0, -2.0], 5)
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 18
}).addTo(map)

// Load JSON data and start the animation
fetch('data.json')
  .then(response => response.json())
  .then(data => {
    animateMovement(data)
  })
  .catch(error => console.error('Error loading the JSON data:', error))

// Create a custom control for displaying the year
const yearControl = L.control({ position: 'bottomleft' })

yearControl.onAdd = function (map) {
  this._div = L.DomUtil.create('div', 'year-display')
  this.update()
  return this._div
}

yearControl.update = function (year) {
  this._div.innerHTML = `<h1>${year}</h1>`
}

// Add the control to the map
yearControl.addTo(map)

// Get the start and end date inputs
const startDateInput = document.getElementById('startDate')
const endDateInput = document.getElementById('endDate')

let earliestDate
let latestDate

// Function to get the timeline bounds
function getTimelineBounds () {
  let startDate = parseInt(startDateInput.value, 10)
  let endDate = parseInt(endDateInput.value, 10)

  startDate = isNaN(startDate) ? 1100 : startDate
  endDate = isNaN(endDate) ? 2023 : endDate

  return { startDate, endDate }
}

let isPaused = false

// Get the play/pause button
const playPauseButton = document.getElementById('playPauseButton')

playPauseButton.addEventListener('click', function () {
  isPaused = !isPaused
  playPauseButton.textContent = isPaused ? 'Resume' : 'Pause'
})

// Get the speed slider
const speedSlider = document.getElementById('speedSlider')

let animationSpeed = 8640000000 // default speed

speedSlider.addEventListener('input', function () {
  animationSpeed = parseInt(speedSlider.value, 10) * 86400000 // Adjust the multiplier as needed
})

function animateMovement (data) {
  // Assuming 'data' is an array of objects similar to the provided sample
  data.sort((a, b) => new Date(a.birth_date) - new Date(b.birth_date))

  let markers = {}

  const { startDate, endDate } = getTimelineBounds()
  earliestDate = new Date(startDate, 0, 1).getTime()
  latestDate = new Date(endDate, 0, 1).getTime()
  let simulatedCurrentTime = earliestDate

  // Add event listeners to both startDateInput and endDateInput
  startDateInput.addEventListener('change', resetAnimation)
  endDateInput.addEventListener('change', resetAnimation)

  function addMarker (person) {
    if (!person.birth_coordinates) {
      return // Skip if birth_coordinates are missing
    }
    if (!person.birth_coordinates || isNaN(person.birth_coordinates.latitude) || isNaN(person.birth_coordinates.longitude)) {
      console.error('Invalid birth coordinates for', person.full_name)
      return // Skip if birth_coordinates are missing or invalid
    }

    const birthMarker = L.circleMarker(
      [person.birth_coordinates.latitude, person.birth_coordinates.longitude],
      { color: 'green', radius: 3 }
    ).addTo(map)

    markers[person.full_name] = {
      marker: birthMarker,
      birthTime: new Date(person.birth_date).getTime(),
      deathTime: person.death_date ? new Date(person.death_date).getTime() : latestDate,
      birthCoordinates: person.birth_coordinates,
      deathCoordinates: person.death_coordinates
    }
  }

  function resetAnimation () {
    const bounds = getTimelineBounds()
    earliestDate = new Date(bounds.startDate, 0, 1).getTime()
    latestDate = new Date(bounds.endDate, 0, 1).getTime()

    simulatedCurrentTime = earliestDate // Reset to the new start date

    // Clear existing markers
    Object.keys(markers).forEach(personName => {
      const personInfo = markers[personName]
      if (personInfo && personInfo.marker) {
        map.removeLayer(personInfo.marker)
      }
    })
    markers = {}

    // Optionally, reset the play/pause button to a default state
    // isPaused = false
    // playPauseButton.textContent = 'Pause'
  }

  function updateMarkerPosition (personName, progress) {
    const personInfo = markers[personName]
    if (!personInfo.deathCoordinates || isNaN(personInfo.deathCoordinates.latitude) || isNaN(personInfo.deathCoordinates.longitude)) {
      console.error('Invalid death coordinates for', personName)
      return // Skip if death_coordinates are invalid
    }
    const birthCoords = personInfo.birthCoordinates
    const deathCoords = personInfo.deathCoordinates

    const lat = birthCoords.latitude + (deathCoords.latitude - birthCoords.latitude) * progress
    const lon = birthCoords.longitude + (deathCoords.longitude - birthCoords.longitude) * progress

    if (isNaN(lat) || isNaN(lon)) {
      console.error('Invalid death coordinates for', personName)
      return // Skip if death_coordinates are invalid
    }
    personInfo.marker.setLatLng([lat, lon])
  }

  function animate () {
    
    requestAnimationFrame(animate)
    if (isPaused) {
        return; // Skip updating the animation if paused
    }
    if (!isPaused) {
        
      // ... existing animation code ...
      simulatedCurrentTime += animationSpeed

      if (simulatedCurrentTime > latestDate) {
        simulatedCurrentTime = earliestDate // Reset to the beginning
        // Clear existing markers to restart the animation
        Object.keys(markers).forEach(personName => {
          const personInfo = markers[personName]
          if (personInfo && personInfo.marker) {
            map.removeLayer(personInfo.marker)
          }
        })
        markers = {} // Clear existing markers to restart the animation
        console.log('Restarting animation')
      }

      const currentYear = new Date(simulatedCurrentTime).getUTCFullYear()
      yearControl.update(currentYear) // Update the year display

      // console.log('Simulated Time: ' + new Date(simulatedCurrentTime).toUTCString())

      // Update and add markers
      data.forEach(person => {
        if (!person.birth_coordinates) {
          return // Skip if birth_coordinates are missing
        }

        const birthTime = new Date(person.birth_date).getTime()
        const deathTime = person.death_date ? new Date(person.death_date).getTime() : latestDate

        if (simulatedCurrentTime >= birthTime && !(person.full_name in markers)) {
          addMarker(person)
        }

        if (person.full_name in markers) {
          let personProgress = (simulatedCurrentTime - birthTime) / (deathTime - birthTime)
          if (personProgress > 1) personProgress = 1
          updateMarkerPosition(person.full_name, personProgress)
        }
      })

      // Remove markers
      Object.keys(markers).forEach(personName => {
        const personInfo = markers[personName]
        if (simulatedCurrentTime >= personInfo.deathTime) {
          map.removeLayer(personInfo.marker)
          delete markers[personName]
        }
      })
    }
  }

  // Start the animation
  requestAnimationFrame(animate)
}
